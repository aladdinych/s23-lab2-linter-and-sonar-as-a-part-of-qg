# Lab 2 -- Linter and SonarQube as a part of quality gates

## Report:

`docker run -d --name sonarqube -e SONAR_ES_BOOTSTRAP_CHECKS_DISABLE=true -p 9000:9000 sonarqube:latest`

it prompted to change password:

`admin` : `1111`

generate token:

![img_1.png](img_1.png)

```bash
mvn clean verify sonar:sonar \
  -Dsonar.projectKey=lab2_nugaev \
  -Dsonar.host.url=http://localhost:9000 \
  -Dsonar.login=...
```

![img.png](img.png)

![img_2.png](img_2.png)

sonarcloud project and org name: `timurnugaev`

after pipeline and sonarcloud job pass, we can see sonarcloud dashboard:

![img_3.png](img_3.png)

